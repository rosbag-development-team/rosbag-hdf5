import sys
import roshdf as rosdb

if __name__ == '__main__':

    bags= ['bags/forward_1600.bag','bags/hydro3.bag']
    
    descriptor = {'distance' : '0.5',
            'configuration' : 'straight'}

    db = rosdb.HDFManager('mydatabase.h5',attributes = descriptor)
    db.store_bags(bags)
    
        
