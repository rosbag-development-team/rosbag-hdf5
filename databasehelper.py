import networkx as nx
import uuid
import matplotlib.pyplot as plt
import rosbag
import rosmsg
import rospy
import tables
import time

ros_primitives = (
        'bool',
        'int8',
        'uint8',
        'int16',
        'uint16',
        'int32',
        'uint32',
        'int64',
        'uint64',
        'float32',
        'float64',
        'string'
        )
chars = 255
data_look_up = {
        'byte'      : tables.UInt8Col,
        'bool'      : tables.BoolCol,
        'float16'   : tables.Float16Col,
        'float32'   : tables.Float32Col,
        'float64'   : tables.Float64Col,
        'uint8'     : tables.UInt8Col,
        'uint16'    : tables.UInt16Col,
        'uint32'    : tables.UInt32Col,
        'uint64'    : tables.UInt64Col,
        'int8'      : tables.Int8Col,
        'int16'     : tables.Int16Col,
        'int32'     : tables.Int32Col,
        'int64'     : tables.Int64Col,
        'int'       : tables.Int32Col,
        }
primitives = (int,float,bool,str)
iterable = (tuple,list)

def clean_msg_def(msg):
    """Strips array tags from definition """
    start = msg.find('[')
    return msg[:start]

def crawl_message(msg,handle,path=''):

    try:
        for slot in msg.__slots__:
            if not path:
                npath = slot
            else:
                npath = '/'.join([path,slot])
            crawl_message(getattr(msg,slot),handle,npath)
    except:
        try:
            handle[path] = msg
        except:
            pass
class Message(object):

    def __init__(self,msg,bag_name,topic):
        self._mGraph = nx.DiGraph()
        self._msg = msg
        self._root = str(uuid.uuid4())
        self._bag = bag_name
        self._topic = topic
        self.done = False
        self._mGraph.add_node(self._root,name='/')
        self._build_message_graph()
        nodes = nx.get_node_attributes(self._mGraph,'mtype')
        self.leafs = [n for n in nodes if self._mGraph.node[n]['mtype'] == 'list' or 
                self._mGraph.node[n]['mtype'] == 'string']
        for leaf in self.leafs:
            self._mGraph.node[leaf]['done'] = False

    def _crawl(self,msg,ncheck):
        try:
            for idx,slot in enumerate(msg.__slots__):
                if slot == self._mGraph.node[ncheck]['name']:
                    node = self._mGraph.node[ncheck]
                    try:
                        if node['size']:
                            node['done'] = True
                    except:
                        node['size'] = len(getattr(msg,slot))
                        node['l_type'] = type(getattr(msg,slot))
                        node['e_type'] = clean_msg_def(msg._slot_types[idx])
                else:
                    self._crawl(getattr(msg,slot),ncheck)
        except:
               pass 

    def _analyze_redux(self,msg):
        for leaf in self.leafs:
            self._crawl(msg,leaf)

        check = [True for n in self.leafs if self._mGraph.node[n]['done']]
        if all(check):
            self.done = True

        
    def _analyze_message(self):
        print('Analysing topic: {}'.format(self._topic))
        bag = rosbag.Bag(self._bag)
        
        nodes = nx.get_node_attributes(self._mGraph,'mtype')
        ln = [n for n in nodes if self._mGraph.node[n]['mtype'] == 'list' or 
                self._mGraph.node[n]['mtype'] == 'string']
        for topic,msg,t in bag.read_messages(topics=self._topic):
            for node in ln:
                self._crawl(msg,node)

    def _add_layer(self,parent,slots,msg):
        for idx,slot in enumerate(slots):
            nSlot = str(uuid.uuid4())
            mParent = getattr(msg,slot)
            self._mGraph.add_node(nSlot,name=slot)
            self._mGraph.add_edge(parent,nSlot)
            try:
                self._add_layer(nSlot,mParent.__slots__,mParent)
            except:
                if msg._slot_types[idx] in ros_primitives:
                    self._mGraph.node[nSlot]['mtype'] = msg._slot_types[idx]

                elif type(getattr(msg,slot)) in iterable:
                    #deal with array
                    self._mGraph.node[nSlot]['mtype'] = 'list'
        
    def _find_structure(self,parent,dic):

        children=self._mGraph.successors(parent)
        for child in children:
            if not self._get_name(child) in dic.keys():
                try:
                    mtype = self._mGraph.node[child]['mtype']
                    if mtype in data_look_up.keys():
                        #The datatype exists
                        dic[self._get_name(child)] = data_look_up[mtype]()
                    elif mtype in 'string':
                        #String is a primitive with size
                        str_size = self._mGraph.node[child]['size']
                        dic[self._get_name(child)] = tables.StringCol(str_size)
                    else:
                        size = self._mGraph.node[child]['size']
                        e_type = self._mGraph.node[child]['e_type']
                        dic[self._get_name(child)] = data_look_up[e_type](shape=size)
                except:
                    dic[self._get_name(child)] = {}
                self._find_structure(child,dic[self._get_name(child)])

    def _check_consistency(self,body):
        """Checks if all dicts have content and removes those that do not"""
        new_body = {}
        for k,v in body.items():
            if isinstance(v,dict):
                v = self._check_consistency(v)
            if not v in (u'',None,{}):
                new_body[k] = v
        return new_body
    
    def generate_table(self):
        """Generates the table body dictionary"""
        msg_body = {}
        self._find_structure(self._root,msg_body)
        msg_body = self._check_consistency(msg_body)
        return type(self._msg,(tables.IsDescription,),msg_body)

    def _get_name(self,uid):
        """Return name in clear text"""
        return self._mGraph.node[uid]['name']

    def _build_message_graph(self):
        msg = rosmsg.get_array_type_instance(self._msg)
        self._add_layer(self._root,msg.__slots__,msg)
        return self._mGraph

    def drawGraph(self):
        pos = nx.spring_layout(self._mGraph)
        nx.draw(self._mGraph,pos)
        node_labels = nx.get_node_attributes(self._mGraph,'name')
        nx.draw_networkx_labels(self._mGraph,pos,labels=node_labels)
        plt.show()

primitives = (int,float,bool,str)
iterable = (tuple,list)

class GraphHandle(object):

    def __init__(self,bag):
        """ Initialise with root as group """
        self._G = nx.DiGraph()
        self._hfroot = str(uuid.uuid4())
        self._root = str(uuid.uuid4())
        self._bag = bag
        self._group_name = bag[bag.rfind('/')+1:bag.find('.bag')].replace('/','_')
        self._G.add_node(self._hfroot,name='/',node_type='group')
        self._G.add_node(self._root,name=self._group_name,node_type='group')
        self._G.add_edge(self._hfroot,self._root)
        self._create_groups(bag)
        self._analyze_rosbag()

    def drawGraph(self):
        pos = nx.spring_layout(self._G)
        nx.draw(self._G,pos)
        node_labels = nx.get_node_attributes(self._G,'name')
        nx.draw_networkx_labels(self._G,pos,labels=node_labels)
        plt.show()
    def _analyze_rosbag(self):

        bag = rosbag.Bag(self._bag)
        tti = bag.get_type_and_topic_info()
        message_dict = {}
        #Create Messages
        for topic in tti.topics:
            message_dict[topic] =Message(tti.topics[topic].msg_type,self._bag,topic)

        done_dict = {k : v.done for k,v in message_dict.items()}
        for topic,msg,t in bag.read_messages():
            if not message_dict[topic].done:
                message_dict[topic]._analyze_redux(msg)
                done_dict[topic] = message_dict[topic].done

            if all(done_dict.values()):
                break

        self.msgs = { '/'+self._group_name+k : v.generate_table() for k,v in message_dict.items()}
        
    def _create_groups(self,bag_name):
        
        bag = rosbag.Bag(bag_name)
        tti = bag.get_type_and_topic_info()
        message_dict = {}
        topics = tti.topics
        for topic in topics:
            groups = topic.split('/')[1:]
            current = self._root
            last = len(groups)-1
            for idx,node_name in enumerate(groups):
                match= [n for n in self._G.successors(current) if self._G.node[n]['name'] == node_name]
                if not match:
                    node = str(uuid.uuid4())
                    if idx == last:
                        self._G.add_node(node,name=node_name,node_type='table')
                    else:
                        self._G.add_node(node,name=node_name,node_type='group')
                    self._G.add_edge(current,node)
                    current = node
                else:
                    #Should never have more than one match
                    assert(len(match) == 1)
                    current = match[0]

    def _find_path(self,node):
        """ Finds path from root to node and returns as a list"""    
        return list(nx.shortest_path(self._G,source=self._hfroot,target=node))

    def _get_name(self,uid):
        """Return name in clear text"""
        return self._G.node[uid]['name']

    def _get_path(self,node):
        """Takes UUID of node and return unique path""" 
        predecessor = self._find_path(node)
        return self._get_name(predecessor[0])+'/'.join([self._get_name(n) for n in predecessor[1:]])

    def write_to_file(self,hf_name):
        hf = tables.open_file(hf_name,'a')
        groups = list(nx.bfs_edges(self._G,self._hfroot))
        grp = hf.root
        for group in groups:
            if not self._get_name(group[1]) in hf.walk_groups():
                path = self._get_path(group[0])
                if path == '/':
                    grp = hf.root
                    tpath = path+self._get_name(group[1])
                else:
                    grp = hf.get_node(path)
                    tpath = path+'/'+self._get_name(group[1])
                if tpath in self.msgs.keys():
                    try:
                        hf.create_table(grp,self._get_name(group[1]),self.msgs[tpath])
                    except:
                        print('{} failed'.format(tpath))
                else:
                    hf.create_group(grp,self._get_name(group[1]))

        hf.close() 

def check_export(hf_name,rbag):

    bag = rosbag.Bag(rbag)

    hf = tables.open_file(hf_name,'r')
    tb = list(hf.walk_nodes(hf.root,'Table'))

    hf_messages = {table._v_pathname : table.nrows for table in tb}

    topics = bag.get_type_and_topic_info().topics
    match = { topic : False for topic in hf_messages.keys()}
    for topic in topics:
        try:
            if hf_messages[topic]:
                print('Topic: {}, HDF: {}, rosbag: {}'.format(topic,hf_messages[topic],
                    topics[topic].message_count))
                if hf_messages[topic] == topics[topic].message_count:
                    match[topic] = True
        except:
            pass

    if all(match.values()):
        print('All messages were exported')
    else:
        print('Missing messages')
    hf.close()

def export_data(hf_name,rbag):

    bag = rosbag.Bag(rbag)
    bagname = rbag[:rbag.find('.')].replace('/','_')

    hf = tables.open_file(hf_name,'a')
    root = hf.root._f_get_child(bagname)
    tb = list(hf.walk_nodes(root,'Table'))
    messages = {k._v_pathname.replace('/'+bagname,'') : k.row for k in tb}

    tti = bag.get_type_and_topic_info()

    for topic in tti.topics.keys():
        if topic in messages.keys():
            print('{} is in table dict'.format(topic))
        else:
            print('{} is not in table dict'.format(topic))
    
    for topic,msg,t in bag.read_messages():
        try:
            crawl_message(msg,messages[topic])
            messages[topic].append()
        except:
            pass

    for table in tb:
        table.flush()
    hf.close()

if __name__ == '__main__':

    t0 = time.time()
    sys= GraphHandle('../test/forward_1600.bag')
    t1 = time.time()
    sys.write_to_file('forward.h5')
    t2 = time.time()
    export_data('forward.h5','../test/forward_1600.bag')
    t3 = time.time()
    print('GraphHandle : {}'.format(t1-t0))
    print('write_to_file : {}'.format(t2-t1))
    print('export_data: {}'.format(t3-t2))
    #GraphHandle : 76.8996510506 s 
    #write_to_file : 0.0886330604553 s 
    #export_data: 6.57427597046 s
    check_export('forward.h5','../test/forward_1600.bag')
