import rosmsg
import rospy
import rosbag
import networkx as nx
import uuid
import tables
from .utils import *
__all__ = [
        'Message',
        'ros_primitives'
        ]

primitives = (int,float,bool,str)
iterable = (tuple,list)
chars = 255
data_look_up = {
        'byte'      : tables.UInt8Col,
        'bool'      : tables.BoolCol,
        'float16'   : tables.Float16Col,
        'float32'   : tables.Float32Col,
        'float64'   : tables.Float64Col,
        'uint8'     : tables.UInt8Col,
        'uint16'    : tables.UInt16Col,
        'uint32'    : tables.UInt32Col,
        'uint64'    : tables.UInt64Col,
        'int8'      : tables.Int8Col,
        'int16'     : tables.Int16Col,
        'int32'     : tables.Int32Col,
        'int64'     : tables.Int64Col,
        'int'       : tables.Int32Col,
        }
ros_primitives = (
        'bool',
        'int8',
        'uint8',
        'int16',
        'uint16',
        'int32',
        'uint32',
        'int64',
        'uint64',
        'float32',
        'float64',
        'string'
        )
class Message(object):

    def __init__(self,msg,bag,topic):
        self._mGraph = nx.DiGraph()
        self._msg = msg
        self._root = str(uuid.uuid4())
        self._bag = bag
        self._topic = topic
        self.done = False
        self._mGraph.add_node(self._root,name='/')
        self._build_message_graph()
        nodes = nx.get_node_attributes(self._mGraph,'mtype')
        self.leafs = [n for n in nodes if self._mGraph.node[n]['mtype'] == 'list' or 
                self._mGraph.node[n]['mtype'] == 'string']
        for leaf in self.leafs:
            self._mGraph.node[leaf]['done'] = False

    def _crawl(self,msg,ncheck):
        try:
            for idx,slot in enumerate(msg.__slots__):
                if slot == self._mGraph.node[ncheck]['name']:
                    node = self._mGraph.node[ncheck]
                    try:
                        if node['size']:
                            node['done'] = True
                    except:
                        node['size'] = len(getattr(msg,slot))
                        node['l_type'] = type(getattr(msg,slot))
                        node['e_type'] = clean_msg_def(msg._slot_types[idx])
                else:
                    self._crawl(getattr(msg,slot),ncheck)
        except:
               pass 

    def _analyze_redux(self,msg):
        for leaf in self.leafs:
            self._crawl(msg,leaf)

        check = [True for n in self.leafs if self._mGraph.node[n]['done']]
        if all(check):
            self.done = True

        
    def _analyze_message(self):
        print('Analysing topic: {}'.format(self._topic))
#        bag = rosbag.Bag(self._bag)
        
        nodes = nx.get_node_attributes(self._mGraph,'mtype')
        ln = [n for n in nodes if self._mGraph.node[n]['mtype'] == 'list' or 
                self._mGraph.node[n]['mtype'] == 'string']
        for topic,msg,t in self._bag.read_messages(topics=self._topic):
            for node in ln:
                self._crawl(msg,node)

    def _add_layer(self,parent,slots,msg):
        for idx,slot in enumerate(slots):
            nSlot = str(uuid.uuid4())
            mParent = getattr(msg,slot)
            self._mGraph.add_node(nSlot,name=slot)
            self._mGraph.add_edge(parent,nSlot)
            try:
                self._add_layer(nSlot,mParent.__slots__,mParent)
            except:
                if msg._slot_types[idx] in ros_primitives:
                    self._mGraph.node[nSlot]['mtype'] = msg._slot_types[idx]

                elif type(getattr(msg,slot)) in iterable:
                    #deal with array
                    self._mGraph.node[nSlot]['mtype'] = 'list'
        
    def _find_structure(self,parent,dic):

        children=self._mGraph.successors(parent)
        for child in children:
            if not self._get_name(child) in dic.keys():
                try:
                    mtype = self._mGraph.node[child]['mtype']
                    if mtype in data_look_up.keys():
                        #The datatype exists
                        dic[self._get_name(child)] = data_look_up[mtype]()
                    elif mtype in 'string':
                        #String is a primitive with size
                        str_size = self._mGraph.node[child]['size']
                        dic[self._get_name(child)] = tables.StringCol(str_size)
                    else:
                        size = self._mGraph.node[child]['size']
                        e_type = self._mGraph.node[child]['e_type']
                        if self._get_name(child) == 'channels':
                            print(size,e_type)
                        dic[self._get_name(child)] = data_look_up[e_type](shape=size)
                except:
                    dic[self._get_name(child)] = {}
                self._find_structure(child,dic[self._get_name(child)])

    def _check_consistency(self,body):
        """Checks if all dicts have content and removes those that do not"""
        new_body = {}
        for k,v in body.items():
            if isinstance(v,dict):
                v = self._check_consistency(v)
            if not v in (u'',None,{}):
                new_body[k] = v
        return new_body
    
    def generate_table(self):
        """Generates the table body dictionary"""
        msg_body = {}
        self._find_structure(self._root,msg_body)
        msg_body = self._check_consistency(msg_body)
        return type(self._msg,(tables.IsDescription,),msg_body)

    def _get_name(self,uid):
        """Return name in clear text"""
        return self._mGraph.node[uid]['name']

    def _build_message_graph(self):
        msg = rosmsg.get_array_type_instance(self._msg)
        self._add_layer(self._root,msg.__slots__,msg)
        return self._mGraph

    def drawGraph(self):
        pos = nx.spring_layout(self._mGraph)
        nx.draw(self._mGraph,pos)
        node_labels = nx.get_node_attributes(self._mGraph,'name')
        nx.draw_networkx_labels(self._mGraph,pos,labels=node_labels)
        plt.show()
