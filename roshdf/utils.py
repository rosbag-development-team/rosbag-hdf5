import rosbag
import tables
import os
__all__ = [
       'detect_meta',
       'clean_msg_def',
       'crawl_message',
       'check_export',
       'export_data'
       ]
def detect_meta(filename):
    """ rosbag -o yields date and time """



    pass

def clean_msg_def(msg):
    """Strips array tags from definition """
    start = msg.find('[')
    return msg[:start]

def crawl_message(msg,handle,path=''):

    try:
        for slot in msg.__slots__:
            if not path:
                npath = slot
            else:
                npath = '/'.join([path,slot])
            crawl_message(getattr(msg,slot),handle,npath)
    except:
        try:
            handle[path] = msg
        except:
            pass

def check_export(hf_name,rbag):

    bag = rosbag.Bag(rbag)

    hf = tables.open_file(hf_name,'r')
    tb = list(hf.walk_nodes(hf.root,'Table'))

    hf_messages = {table._v_pathname : table.nrows for table in tb}

    topics = bag.get_type_and_topic_info().topics
    match = { topic : False for topic in hf_messages.keys()}
    for topic in topics:
        try:
            if hf_messages[topic]:
                print('Topic: {}, HDF: {}, rosbag: {}'.format(topic,hf_messages[topic],
                    topics[topic].message_count))
                if hf_messages[topic] == topics[topic].message_count:
                    match[topic] = True
        except:
            pass

    if all(match.values()):
        print('All messages were exported')
    else:
        print('Missing messages')
    hf.close()

def export_data(hf,bag):

    bag_name = os.path.split(bag.filename)[-1]
    groups = [hf.root._f_get_child(group) for group in hf.root._v_groups]
    bags = {group._v_attrs['filename'] : group._v_name for group in groups}

    root = hf.root._f_get_child(bags[bag_name])
    tb = list(hf.walk_nodes(root,'Table'))
    messages = {k._v_pathname.replace('/'+bags[bag_name],'') : k.row for k in tb}

    tti = bag.get_type_and_topic_info()

    for topic in tti.topics.keys():
        if topic in messages.keys():
            print('{} is in table dict'.format(topic))
        else:
            print('{} is not in table dict'.format(topic))
    
    for topic,msg,t in bag.read_messages():
        try:
            crawl_message(msg,messages[topic])
            messages[topic].append()
        except:
            pass

    for table in tb:
        table.flush()
