import networkx as nx
import uuid
import matplotlib.pyplot as plt
import rosbag
import rosmsg
import rospy
import tables
import time
import datetime
#from .messagehandler import Message
#from .utils import detect_meta, clean_msg_def, crawl_message 
from .utils import * 
from .messagehandler import *

__all__ = [
#%'HDFManager',
'GraphHandle',
'export_data',
'check_export'
]


def file_name_cleaner(name):
    idx_s = f.rfind('_')+1
    idx_e = f.rfind('.')
    f_strip = f[idx_s:idx_e]
    try:
        data = datetime.datetime.strptime(f_strip,'%Y-%m-%d-%H-%M-%S')
        return f_strip
    except:
        pass


class GraphHandle(object):

    def __init__(self,bag,name):
        """ Initialise with root as group """
        self._G = nx.DiGraph()
        self._hfroot = str(uuid.uuid4())
        self._root = str(uuid.uuid4())
        self._bag = bag
#        self._group_name = bag[bag.rfind('/')+1:bag.find('.bag')].replace('/','_')
        self._group_name = name 
        self._G.add_node(self._hfroot,name='/',node_type='group')
        self._G.add_node(self._root,name=self._group_name,node_type='group')
        self._G.add_edge(self._hfroot,self._root)
        self._create_groups()
        self._analyze_rosbag()

    def drawGraph(self):
        pos = nx.spring_layout(self._G)
        nx.draw(self._G,pos)
        node_labels = nx.get_node_attributes(self._G,'name')
        nx.draw_networkx_labels(self._G,pos,labels=node_labels)
        plt.show()
    def _analyze_rosbag(self):

#        bag = rosbag.Bag(self._bag)
        tti = self._bag.get_type_and_topic_info()
        self._stamp = self._bag.get_start_time()
        message_dict = {}
        #Create Messages
        for topic in tti.topics:
            message_dict[topic] = Message(tti.topics[topic].msg_type,self._bag,topic)

        done_dict = {k : v.done for k,v in message_dict.items()}
        for topic,msg,t in self._bag.read_messages():
            if not message_dict[topic].done:
                message_dict[topic]._analyze_redux(msg)
                done_dict[topic] = message_dict[topic].done

            if all(done_dict.values()):
                break

        self.msgs = { '/'+self._group_name+k : v.generate_table() for k,v in message_dict.items()}
        
    def _create_groups(self):
        
#        bag = rosbag.Bag(bag_name)
        tti = self._bag.get_type_and_topic_info()
        topics = tti.topics
        for topic in topics:
            groups = topic.split('/')[1:]
            current = self._root
            last = len(groups)-1
            for idx,node_name in enumerate(groups):
                match= [n for n in self._G.successors(current) if self._G.node[n]['name'] == node_name]
                if not match:
                    node = str(uuid.uuid4())
                    if idx == last:
                        self._G.add_node(node,name=node_name,node_type='table',message=topics[topic].msg_type)
                    else:
                        self._G.add_node(node,name=node_name,node_type='group')
                    self._G.add_edge(current,node)
                    current = node
                else:
                    #Should never have more than one match
                    assert(len(match) == 1)
                    current = match[0]

    def _find_path(self,node):
        """ Finds path from root to node and returns as a list"""    
        return list(nx.shortest_path(self._G,source=self._hfroot,target=node))

    def _get_name(self,uid):
        """Return name in clear text"""
        return self._G.node[uid]['name']

    def _get_path(self,node):
        """Takes UUID of node and return unique path""" 
        predecessor = self._find_path(node)
        return self._get_name(predecessor[0])+'/'.join([self._get_name(n) for n in predecessor[1:]])

    def write_to_file(self,hf):
#        hf = tables.open_file(hf_name,'a')
        groups = list(nx.bfs_edges(self._G,self._hfroot))
        grp = hf.root
        for group in groups:
            if not self._get_name(group[1]) in hf.walk_groups():
                path = self._get_path(group[0])
                if path == '/':
                    grp = hf.root
                    tpath = path+self._get_name(group[1])
                else:
                    grp = hf.get_node(path)
                    tpath = path+'/'+self._get_name(group[1])
                if tpath in self.msgs.keys():
                    try:
                        tab = hf.create_table(grp,self._get_name(group[1]),self.msgs[tpath])
                        tab.attrs.msg_type = self._G.node[group[1]]['message']

                    except Exception as ext:
                        print(self,self._get_name(group[1]),ext)
                else:
                    hf.create_group(grp,self._get_name(group[1]))
        #Add Time stamps and filename attribute
#        grp = hf.root._f_get_child(self._group_name)
#        grp._v_attrs.filename = self._bag.
#        grp._v_attrs.stamp = self._stamp
        
#        hf.close() 


if __name__ == '__main__':

    t0 = time.time()
    sys= GraphHandle('../test/forward_1600.bag')
    t1 = time.time()
    sys.write_to_file('forward.h5')
    t2 = time.time()
    export_data('forward.h5','../test/forward_1600.bag')
    t3 = time.time()
    print('GraphHandle : {}'.format(t1-t0))
    print('write_to_file : {}'.format(t2-t1))
    print('export_data: {}'.format(t3-t2))
    #GraphHandle : 76.8996510506 s #write_to_file : 0.0886330604553 s 
    #export_data: 6.57427597046 s
    check_export('forward.h5','../test/forward_1600.bag')
