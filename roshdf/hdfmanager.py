import tables
import rosmsg
import rospy
import rosbag
import re
import os
import datetime
from .baghandler import *

__all__ = [
        'HDFManager'
        ]
class HDFManager(object):


    def __init__(self,hf_name,prefix = 'mission',attributes={}):
        
        self._hf_name = hf_name
        self._prefix = prefix
        self._attributes = attributes

    def store_bags(self,bags):

        try:
            hf = tables.open_file(self._hf_name,'r+')
            start,bags = self._sort_bags(hf,bags)
            print('Appending bags: {}'.format(bags))
        except:
            print('File {} does not exists'.format(self._hf_name))
            hf = tables.open_file(self._hf_name,'a')
            start = 1

        for idx,bag_name in enumerate(bags,start):
            name = self._prefix + str(idx).zfill(3)
            bag = rosbag.Bag(bag_name)
            G = GraphHandle(bag,name)
            G.write_to_file(hf)
            grp = hf.root._f_get_child(name)
            self._add_attributes(grp,bag)
            export_data(hf,bag)
        hf.close()

    def _add_attributes(self,group,bag):
        filename = os.path.split(bag.filename)[-1]
        stamp = datetime.datetime.fromtimestamp(bag.get_start_time())
        #Update dictionary
        self._attributes.update({
            'filename' : filename,
            'stamp' : stamp.strftime('%Y-%m-%d-%H-%M-%S')
            })

        for key in self._attributes.keys():
            group._f_setattr(key,self._attributes[key])


    def _sort_bags(self,hf,bags):
        #root groups
        groups = hf.root._v_groups
        subgrps = [hf.root._f_get_child(group) for group in groups]

        files_in_hdf = [group._v_attrs['filename'] for group in subgrps]

        bags_to_add = [bag for bag in bags if not os.path.split(bag)[-1] in files_in_hdf]

        print(bags_to_add)
        lmission = sorted(groups.keys())[-1]
        start = map(int,re.findall('\d+',lmission))

        return start[0]+1,bags_to_add


        
#        for idx,bag in enumerate(bags,1):
#            G = GraphHandle(bag,prefix+str(idx).zfill(3),clean(bag))
#            G.write_to_file(hf_name)
#            export_data(hf_name,bag)
#
#        self._hf.close()
#        #Check if bag exists
#        #If bag exists check groups.attr.filename
#        #
#        hf.root._g_check_not_contains


